<?php declare(strict_types=1);

function reverseArray($arr): array {
    $reversedArr = [];

    for($i = count($arr) - 1; $i >= 0; $i--) {
        array_push($reversedArr,$arr[$i]);
    }

    return $reversedArr;
}

$arr = ['Apple', 'Banana', 'Orange', 'Coconut'];
echo join(', ', reverseArray($arr));