<?php declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Http\Entities\Facility;

class FacilityTest extends TestCase
{

    public function testCanSlugifyRouteParams() {
        $facility = new Facility();
        $facility->setState('California');
        $facility->setCounty('Some County');
        $facility->setCity('Los Angeles');
        $facility->setLocationCode('999_999');

        var_dump($facility->getRouteParams());

        $expect = [
            'state' => 'california',
            'county' => 'some-county-county',
            'city' => 'los-angeles',
            'location_code' => '999-999'
        ];

        $this->assertEquals($expect,$facility->getRouteParams());
    }
}