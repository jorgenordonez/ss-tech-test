<?php

namespace App\Repositories;

use App\Models\Payment;

class PaymentRepository
{
    public function getAll(array $reqOptions): ?array
    {
        $payments = null;

        $limit = $reqOptions['limit'] ?? 20;

        if ($reqOptions['sortDir'] === 'desc') {
            $payments = Payment::sortByDesc('id')->limit($limit);
        }
        
        $payments = Payment::limit($limit); // table with roughly 1.2m records
        $return = [];

        if (!count($payments)) {
            return $return;
        }



        // for ($i = 0; $i < $reqOptions['limit'] ?? 20; $i++) {
            // $pmt = $payments[$i]->toArray();

            // $return[] = array_merge($pmt, [
            //     'facility' => $payments[$i]->facility,
            //     'user' => $payments[$i]->user,
            // ]);
        // }

        // return $return;
        
        return $payments->load('facility','user');
    }
}
