<?php

namespace App\Http\Controllers;

use App\Repositories\PaymentRepository;
use Illuminate\Http\JsonResponse;

class PaymentController extends Controller
{
    protected $paymentRepository;

    public function __construct(PaymentRepository $paymentRepository)
    {
        $this->paymentrepository = $paymentRepository;
    }

    public function index(Request $request): JsonResponse
    {
        $payments = $this->paymentrepository->getAll($request->all());

        return new JsonResponse(['payments' => $payments]);
    }
}
